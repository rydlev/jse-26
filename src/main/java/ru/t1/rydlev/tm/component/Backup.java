package ru.t1.rydlev.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.command.data.AbstractDataCommand;
import ru.t1.rydlev.tm.command.data.DataBackupLoadCommand;
import ru.t1.rydlev.tm.command.data.DataBackupSaveCommand;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    public void init() {
        load();
        start();
    }

    public void save() {
        bootstrap.parseCommand(DataBackupSaveCommand.NAME, false);
    }

    public void load() {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP))) return;
        bootstrap.parseCommand(DataBackupLoadCommand.NAME, false);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Thread.sleep(5000);
            save();
        }
    }

}
