package ru.t1.rydlev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.exit(0);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Close application.";
    }

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

}
