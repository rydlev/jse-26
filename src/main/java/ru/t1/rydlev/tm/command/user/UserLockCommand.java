package ru.t1.rydlev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.enumerated.Role;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User lock.";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-lock";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
